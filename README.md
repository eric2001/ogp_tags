# OGP Tags

## Description
This module generates Open Graph META tags for each page in your gallery, which allows sites like facebook to quickly extract information like descriptions and photos, when people post a link to the site. This module is similar to the "facebook_opengraph" module, except that it generates additional tags to allow videos in Gallery to be embeded directly into the facebook news feed.

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
To install, extract the "ogp_tags" folder from the zip file into your Gallery 3 modules folder. Afterwards log into your Gallery web site as an administrator and activate the module in the Admin -> Modules menu.

## History
**Version 1.0.0:**
> - Initial Release
> - Released on 30 July 2013
>
> Download: [Version 1.0.0](/uploads/72f0536519210da45b6638f6dcac1d35/ogp_tags100.zip)
